import DataExtractor
import DataAnalyser

class Calculate:
    def __init__(self,dat_data,col1,col2):
        self.data = dat_data
        self.col1 = col1
        self.col2 = col2
        e = DataExtractor.Data_Extractor()
        a = DataAnalyser.DataAnalyser()

        data1= e.Data_Read(self.data)
        data2 = a.minimum_difference(data1,col1,col2)
        print(data2)
