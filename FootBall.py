import re
import sys

def Data_Coll(file_data):
    
    Football_Data = []
    for data in file_data:
        if(data == '\n'):
            continue
        Football_Data.append(data.strip().split())
    for i in range(1,len(Football_Data)):
        for j in range(len(Football_Data[i])):
            if j == 1:
                continue
            n = re.search(r"\d+",Football_Data[i][j])
            if n != None:
                Football_Data[i][j] = int(n[0])
    #print(Football_Data)
    return Football_Data



def Smallest_diff(Football_Data):
    smallest_diff = sys.maxsize
    team_name = ""

    for i in range(1,len(Football_Data)):
        if i == 18:
            continue
        if  smallest_diff > abs(Football_Data[i][6] - Football_Data[i][8]):
            smallest_diff = abs(Football_Data[i][6] - Football_Data[i][8])
            team_name = Football_Data[i][1]
        

    return team_name, smallest_diff


def main():
    file = open('football.dat','r')
    file_data = file.readlines()
    Data = Data_Coll(file_data)

    print(Smallest_diff(Data))



if __name__ == "__main__":
    main()


    


