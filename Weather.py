import re
import sys


def data_collection():
    file = open('weather.dat', 'r')

    file_data = file.readlines()

    data = []

    for line in file_data:
        if line == '\n':
            continue
        data.append(line.strip().split())

    for i in range(1, len(data)):
        for j in range(4):
            n = re.findall(r"\d+", data[i][j])
            if n != []:
                data[i][j] = int(n[0])
   # print(data)
    return data


def sallest_temp_spread(data):
    smallest_temprature_spread = sys.maxsize

    day_number = 0

    for line in data[1:]:
        if smallest_temprature_spread > int(line[1]) - int(line[2]):
            smallest_temprature_spread = int(line[1]) - int(line[2])
            day_number = line[0]

    return day_number, smallest_temprature_spread


def main():
    data = data_collection()

    # print(type(data[2][1]))

    print(sallest_temp_spread(data))


if __name__ == "__main__":
    main()
